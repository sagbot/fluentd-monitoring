# fluentd-monitoring

This is a small demo of how you can use fluentd to monitor an http endpoint health status.

This demo does:
1. Poll an http endpoint every 1 scond
2. Collect metadata about the request (response code, duration, speed...)
3. Adds additional static user defined fields to the event
4. Runs a python script to handle every event
5. Prints the event to the console output

## How to run
```sh
docker-compose down
docker-compose up --build
```

## How does this demo work

```mermaid
graph LR
A[Fluentd exec input] -- Server polled every x sec --> S[Web server]
A --> B(Fluentd add fields)
B --> C(Fluentd copy event)
C --> D[console output]
C --> E[ptyhon script]
```

### Polling http server
This is done using the linux `curl` command. it can output a ton of metadata about the request using the `--write-out` flag. this command is excecuted by running the `scripts/poll.sh` script

### Adding fields to the request
In the `fluent.conf` file the second configuration section adds a bunch of fields for every request. some of them have static values, & some of them are dynamic.

You can add dynamic values from build in values that are given by fluentd, or use simple `ruby` syntax for custom logic (see the `somthing_wrong` field for example)

### Sending output to multiple destinations
This is done using the output type `copy`. you can read more about it [here](https://docs.fluentd.org/output/copy)

### Sending output to python script
There are two ways you can output fluentd events:
1. Send them immediately as they come.

    This is good because events are handled immediately, but you might encounter performance issues when sending large volumes of events.

2. Send them in batches, every X seconds (This is an oversimplification).

    This is good for performance when sending large volumes of events, but will cause latency between the time the event is created to the time that it is sent.

## Notes
This is a very simple demo, you can change it as you please. for example:
* You can change the command that is ran every interval to do somthing else, for example running a python script
* You can change the polling interval to a slower / faster rate
* You can add custom / change fields of the event straight in the `fluent.conf` file (there are some examples for that in the config file)
* Filter out unwanted events so you only send events you care about
* Change the python output script to do some other custom action (send event to server, write to disk...)
* Send events to other built in output types (you don't have to write everything on your own...). example output types:
    - send an http post of every event
    - write all events to a text file
    - send events to another fluentd instance
    - send events to a `syslog` server
    - send events to a `kafka` server
    - write events to a `mongodb` instance
    - send events to `splunk`