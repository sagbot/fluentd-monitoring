#!/bin/sh

# Poll the http endpoint and save the request metadata as a report
report=$(curl --connect-timeout $TIMEOUT --write-out '@/polling_format' $URL --silent --output /dev/null);   

# Save the curl command exit code
exit_code=$(echo $?);

# Add curl exit code to the report
report=$(echo $report | jq --arg exit_code $exit_code '. + {exitcode: $exit_code}');

# print the report json
echo $report;

# Uncomment this to print report json in a pretty format
# echo $report | jq .;