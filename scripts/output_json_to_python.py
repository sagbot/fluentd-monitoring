# this script is run with the first positional argument being the file containing the events
# for example: python3 output_json_to_python.py /path/to/data.txt
# And data.txt contains:
#   {"key1": "value1", "key2", "value2"}
#   {"key1": "value1", "key2", "value2"}

import sys
import json

import requests

# Get the path of the file containing all the events this script has to process
input_data_path = sys.argv[-1]

with open(input_data_path, 'r') as input_data:

    # Read all the events from the file (line == event)
    for line in input_data.readlines():

        # Load the event record to json format
        record = json.loads(line)
        
        # Send a request to a server
        requests.get('https://www.google.com')

        # Write the raw event to file with some other params
        # every line here looks like: ['/scripts/output_json_to_python.py', '/tmp/fluent-plugin-out-exec-20210208-15-1g5clbg'] -> LINE_CONTENT_HERE
        with open('/out/basic_output.txt', 'a') as output:
            output.write(f'{str(sys.argv)} -> {line}\n')

        # Write a line for each event, using only some fields from the event
        # every line here looks like: 2021-02-08 19:35:28 +0000 -> error: False
        with open('/out/timestaps_output.txt', 'a') as output:
            output.write(f'{record["time"]} -> error: {record["somthing_wrong"]}\n')
