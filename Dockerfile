FROM fluentd:v1.9.1-debian-1.0

USER root
RUN apt update && apt install -y curl jq python3.7 python3-pip
RUN python3.7 -m pip install requests

COPY fluent.conf /fluentd/etc/

COPY polling_format /
COPY scripts /scripts
RUN chmod +x /scripts/*

USER fluent